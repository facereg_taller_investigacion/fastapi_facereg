import os
import time

class Utils:

    @classmethod
    def create_directory(cls, directoryPath):        
        if not os.path.exists(directoryPath):
            os.mkdir(directoryPath)
            time.sleep(1)
            