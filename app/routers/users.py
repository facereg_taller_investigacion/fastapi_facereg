import logging
import os
import traceback
from fastapi import APIRouter, HTTPException,status
from fastapi import responses
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from app.models import BiometricoInfo, BiometricoAcceso, UsuarioTec
from app.controllers.UsersController import UsersController;
from starlette.requests import Request

router = APIRouter();
usersController = UsersController();

@router.get('/usuarios/{id}')
def obtener_usuarios(id: str):
    try:
        return usersController.obtener_usuario(id);
    except Exception as ex:
        logging.error(ex);
        logging.error(traceback.format_exc())
        return JSONResponse(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            content=jsonable_encoder({"detail": 'ERROR DE CONEXIÓN', "body": str(ex)}),
        )

@router.get('/usuarios/numcontrol/{numcontrol}')
def obtener_usuarios_numcontrol(numcontrol: str):
    try:
        return usersController.obtener_usuario_numcontrol(numcontrol);
    except Exception as ex:
        logging.error(ex);
        logging.error(traceback.format_exc())
        return JSONResponse(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            content=jsonable_encoder({"detail": 'ERROR DE CONEXIÓN', "body": str(ex)}),
        )

@router.post('/usuarios')
def guardar_usuario(usuario: UsuarioTec):
    try:
        response = usersController.registrar_usuario(usuario);
        return response
    except Exception as ex:
        logging.error(ex);
        logging.error(traceback.format_exc())
        return JSONResponse(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            content=jsonable_encoder({"detail": 'ERROR DE CONEXIÓN', "body": str(ex)}),
        )

@router.put('/registrar_rostro')
def registrar_rostro(dataRec: BiometricoInfo, request: Request):
    try:        
        print(request.url)
        response = usersController.registrar_rostro(dataRec);          
        return response
    except Exception as ex:
        logging.error(ex);
        logging.error(traceback.format_exc())
        return JSONResponse(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            content=jsonable_encoder({"detail": 'ERROR DE CONEXIÓN', "body": str(ex)}),
        )

@router.post('/reconocimiento_usuario')
def reconocimiento_usuario(dataRec: BiometricoAcceso):
    try:
        response = usersController.reconocimiento_usuario(dataRec);
        return response
    except Exception as ex:
        logging.error(ex);
        logging.error(traceback.format_exc())
        return JSONResponse(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            content=jsonable_encoder({"detail": 'ERROR DE CONEXIÓN', "body": str(ex)}),
        )

@router.get('/files_names')
def files_names():
    data = []
    for dirpath, dnames, fnames in os.walk("."):
        for f in fnames:
                data.append(
                    {
                        "Directory path: ": dirpath,
                        "Directory name: ": (dnames),
                        "File: ":str(f)
                    }
                )
    return data;    