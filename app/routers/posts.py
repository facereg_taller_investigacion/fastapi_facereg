from fastapi import APIRouter
from ..models import Post

router = APIRouter()
posts = []

@router.get('/posts')
def get_posts():
    return posts

@router.get('/posts/{id}')
def show_post(id: str):
    print(posts)
    postFind = [post for post in posts if post['id']==id]
    if(len(postFind) > 0):
        postFind = postFind[0]        
    return postFind

@router.post('/posts')
def save_posts(post: Post):
    posts.append(post.dict());
    return "saved"