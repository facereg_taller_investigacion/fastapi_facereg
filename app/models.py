from pydantic import BaseModel;
from typing import List, Optional, Text;
from datetime import date, datetime;

#Post Model
class Post(BaseModel):
    id: Optional[str]
    title: str
    author: str
    content: Text
    created_at: datetime = datetime.now()
    published_at: Optional[datetime]
    published: bool = False

#User Model
class UsuarioTec(BaseModel):
    numControl: Optional[str]
    nombre: str
    apellidos: str
    email: Text
    snVisitante: bool
    created_at: Optional[datetime]

#Biometricos Model
class BiometricoInfo(BaseModel):
    id: Optional[int]
    numControl: Optional[int]    
    base64Image: str

class BiometricoAcceso(BaseModel):
    id: str
    numControl: Optional[str]    
    base64Image: str
    snEntrada: bool