# from typing import Text;
from fastapi import FastAPI, requests, status;
from dotenv import load_dotenv
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import RequestValidationError
from fastapi.responses import JSONResponse
from fastapi.middleware.cors import CORSMiddleware
from .routers import users, posts;
import logging;

load_dotenv()

logginHandler = logging.FileHandler(filename="app/log/face_reg.log", encoding='utf-8', mode='+w');
logging.basicConfig(handlers=[logginHandler], level=logging.DEBUG)

app = FastAPI();
app.router.prefix = '/api'

app.include_router(users.router)
app.include_router(posts.router)

origins = [    
    "http://localhost",
    "http://localhost:8080",
    "https://localhost:4200",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request: requests, exc: RequestValidationError):
    return JSONResponse(
        status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
        content=jsonable_encoder({"detail": exc.errors(), "body": exc.body}),
    )

@app.get('/')
def welcome_test():
    return {
        "welcome": "data REST"
    };