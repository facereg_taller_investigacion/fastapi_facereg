import os;
from logging import error
import logging
import pymysql.cursors;
from dotenv import load_dotenv

from app.models import BiometricoAcceso, UsuarioTec;

class DBConnection:    

    def __init__(self) -> None:
        load_dotenv()
        env = os.environ;        

        self.dbhost= env.get('DB_HOST'); 
        self.dbuser= env.get('DB_USERNAME'); 
        self.dbpassword= env.get('DB_PASSWORD');
        self.dbname= env.get('DB_DATABASE'); 
        self.dbport = env.get('DB_PORT');        

    def obtener_conexion(self):
        # dbcursor= pymysql.cursors.DictCursor;        
        connection = None;
        try:                
            connection = pymysql.connect(host = self.dbhost,user = self.dbuser,
            password = self.dbpassword, db = self.dbname, port = int(self.dbport),
            cursorclass=pymysql.cursors.DictCursor);

            return connection;
        except Exception as ex:
            if(connection is not None):
                connection.close()
            print(ex)

    def obtener_usuario_id(self,connection,id):

        if(connection is None):
            raise ConnectionError

        try:
            with connection:
                with connection.cursor() as cursor:
                    # Read a single record                    
                    sql = """SELECT id, numControl, nombre, apellidos, email, snVisitante, 
                             created_at FROM usuarios WHERE id=%(id)s"""
                    cursor.execute(sql, {'id': id})
                    result = cursor.fetchone()
                    if(result is None):
                        return result

                    result['snVisitante'] = self.convert_to_bool(result['snVisitante']);                    
                    return result;
        except Exception as ex:
            # if(connection is not None):
            #     connection.close()
            raise ex

    def obtener_usuario_numcontrol(self,connection,numcontrol):

        if(connection is None):
            raise ConnectionError

        try:
            with connection:
                with connection.cursor() as cursor:
                    # Read a single record                    
                    sql = """SELECT id, numControl, nombre, apellidos, email, snVisitante, 
                             created_at FROM usuarios WHERE numControl=%(numControl)s"""
                    cursor.execute(sql, {'numControl': numcontrol})
                    result = cursor.fetchone()
                    if(result is None):
                        return result

                    result['snVisitante'] = self.convert_to_bool(result['snVisitante']);                    
                    return result;
        except Exception as ex:
            # if(connection is not None):
            #     connection.close()
            raise ex
        
    def registrar_usuario(self,connection,usuario: UsuarioTec):

        if(connection is None):
            raise ConnectionError

        try:
            with connection:
                with connection.cursor() as cursor:

                    if(int(usuario.numControl) != 0):
                        sqlQuery = "SELECT id FROM usuarios WHERE numControl=%(numcontrol)s;"
                        cursor.execute(sqlQuery, {'numcontrol': usuario.numControl});
                        result = cursor.fetchone()

                        if(result is not None):
                            raise RuntimeError('El número de control indicado ya ha sido registrado')                                           

                    sqlInsert = """INSERT INTO usuarios(numControl, nombre, apellidos, email, snVisitante)
                             VALUES(%(numControl)s, %(nombre)s,%(apellidos)s, %(email)s,%(snVisitante)s);                             
                             """
                    cursor.execute(sqlInsert, {'numControl': int(usuario.numControl), 'nombre': usuario.nombre, 
                                'apellidos': usuario.apellidos, 'email': usuario.email, 'snVisitante': usuario.snVisitante});
                    connection.commit()

                    sqlQuery = "SELECT LAST_INSERT_ID();"
                    cursor.execute(sqlQuery, {});
                    result = cursor.fetchone() 

                    return result;
        except Exception as ex:
            # if(connection is not None):
            #     connection.close() 
            raise ex

    def registrar_acceso_usuario(self,connection, dataAcceso: BiometricoAcceso, fechaAcceso):
        if(connection is None):
            raise ConnectionError

        try:
            with connection:
                with connection.cursor() as cursor:                          

                    sqlInsert = """INSERT INTO accesos_usuarios(idUsuario, fechaAcceso, snEntrada)
                                   VALUES(%(idUsuario)s, %(fechaAcceso)s, %(snEntrada)s) 
                             """
                    cursor.execute(sqlInsert, {'idUsuario': dataAcceso.id, 'fechaAcceso': fechaAcceso,
                                'snEntrada': dataAcceso.snEntrada});
                    connection.commit()

                    sqlQuery = "SELECT LAST_INSERT_ID();"
                    cursor.execute(sqlQuery, {});
                    result = cursor.fetchone() 

                    return {'data': result};
        except Exception as ex:
            # if(connection is not None):
            #     connection.close() 
            raise ex
        pass

    def convert_to_bool(self, bstring):
        return bstring == b'\x01'