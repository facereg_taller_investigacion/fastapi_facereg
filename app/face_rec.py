import os
import face_recognition
import numpy as np;
from PIL import Image;
from time import sleep

class FaceRec:

    # def __init__(cls, known_person_path_file, unknown_images_path_file, known_name=None):
    #     cls.known_person_path_file = known_person_path_file
    #     cls.unknown_images_path_file = unknown_images_path_file
    #     cls.known_name = known_name
    
    @classmethod
    def find_face_image(cls, imageBase64):
        imageFace = face_recognition.load_image_file(imageBase64)
        face_locations = face_recognition.face_locations(imageFace)        

        for face_location in face_locations:
            # Print the location of each face in this image
            top, right, bottom, left = face_location
            print("A face is located at pixel location Top: {}, Left: {}, Bottom: {}, Right: {}".format(top, left, bottom, right))

            # You can access the actual face itself like this:
            face_image = imageFace[top:bottom, left:right]                              
            pil_image = Image.fromarray(face_image)
            return pil_image

        return None

    @classmethod
    def recognize_face(cls, unknownImg, known_img_path):
        
        faceIsRecongnized = False
        num_known_images = os.listdir(known_img_path)

        if( len(num_known_images) == 0):
            return faceIsRecongnized

        known_face_encodings = []
        known_face_names = []

        print('FILES: '+known_img_path)
        for file in os.listdir(known_img_path):            
            if file[0] != '.':
                print('------------------------')
                print('ACTUAL FILE: '+file)                
                # unknown_face_names = [unknown_name]
                
                known_image = face_recognition.load_image_file(known_img_path + '/' + file)
                face_locations = face_recognition.face_locations(known_image)
                face_encodings = face_recognition.face_encodings(known_image, face_locations)[0]

                known_face_encodings.append(face_encodings)
                known_face_names.append(file)
                print('------------------------')              

        unknown_image = face_recognition.load_image_file(unknownImg)
        unknown_face_locations = face_recognition.face_locations(unknown_image)
        unknown_face_encodings = face_recognition.face_encodings(unknown_image, unknown_face_locations)        

        for (top, right, bottom, left), face_encoding in zip(face_locations, unknown_face_encodings):

            matches = face_recognition.compare_faces(known_face_encodings, face_encoding)            
            print('MATCH: '+', '.join(str(v) for v in matches))        

            face_distances = face_recognition.face_distance(known_face_encodings, face_encoding)
            best_match_index = np.argmin(face_distances)

            print('Best match index')
            print(best_match_index)

            if matches[best_match_index]:
                name = known_face_names[best_match_index]
                print('BEST MATCH FILE: '+name)
            
            face_matches = list(filter(lambda x: x == True, matches))
            print( 'FACE MATCHES: '+', '.join(str(v) for v in face_matches) )

            if(len(face_matches) > 0):
                faceIsRecongnized = True

        print( 'FACE IS RECOGNIZED: '+str(faceIsRecongnized))
        return faceIsRecongnized


