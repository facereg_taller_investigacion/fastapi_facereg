from datetime import datetime
import os
import time
import io

from app.Utils.utils import Utils;
from app.DBConnection import DBConnection;
from app.models import BiometricoInfo, BiometricoAcceso, UsuarioTec;
from PIL import Image;
import base64;
import tempfile;
from app.face_rec import FaceRec;
# import cv2;

class UsersController:

    dbManager = DBConnection();

    def obtener_usuario(self,id):
        connection = self.dbManager.obtener_conexion()
        result = self.dbManager.obtener_usuario_id(connection, id)
        return {'data': result};
    
    def obtener_usuario_numcontrol(self,numControl):
        connection = self.dbManager.obtener_conexion()
        result = self.dbManager.obtener_usuario_numcontrol(connection, numControl)
        return {'data': result};

    def registrar_usuario(self, data: UsuarioTec):
        connection = self.dbManager.obtener_conexion()
        result = self.dbManager.registrar_usuario(connection, data)

        connectionUsuario = self.dbManager.obtener_conexion()
        print(result)
        resultUsuario = self.dbManager.obtener_usuario_id(connectionUsuario, result['LAST_INSERT_ID()'])

        return {'data': resultUsuario};

    def registrar_rostro(self, dataUser: BiometricoInfo):
        connection = self.dbManager.obtener_conexion()
        self.inicializar_directorios_registro()

        print('REGISTRAR ROSTRO')
        user = self.dbManager.obtener_usuario_id(connection, dataUser.id)

        if(user is None):
            raise RuntimeError('Debe registrar al usuario con el id indicado antes de poder guardar una foto.')
        
        try:
            buffer = bytes(dataUser.base64Image, 'utf-8')
            imageBase64 = buffer[buffer.find(b'/9'):]
            image = io.BytesIO(base64.b64decode(imageBase64))

            faceImage = FaceRec.find_face_image(image)

            if faceImage is None:
                raise RuntimeError('No se ha detectado ningún rostro en la foto. Favor de volver a intentarlo.')

            # current date and time
            now = datetime.now()
            current_time = now.strftime("%Y%m%d%H%M%S")            

            if user['snVisitante']:
                pathImageSave = f"app/faces/visitantes/{dataUser.id}"
                Utils.create_directory(pathImageSave)
                absPath = os.path.abspath(f'{pathImageSave}')

                faceImage.save(f'{absPath}/{dataUser.id}_{current_time}.jpg');
            else:
                pathImageSave = f"app/faces/usuarios/{dataUser.numControl}"
                Utils.create_directory(pathImageSave)
                absPath = os.path.abspath(pathImageSave)

                faceImage.save(f'{absPath}/{dataUser.numControl}_{current_time}.jpg')

            return {'data': 
            {'msg':'Imagen guardada exitosamente.',
             'image': dataUser.base64Image}
            }
        except Exception as ex:
            if(faceImage is not None):
                faceImage.close()

            raise ex;

    def reconocimiento_usuario(self, dataUser: BiometricoAcceso):
        connection = self.dbManager.obtener_conexion()
        self.inicializar_directorios_registro()

        faceImage = None
        user = self.dbManager.obtener_usuario_id(connection, dataUser.id)

        if(user is None):
            raise RuntimeError('Debe registrar al usuario con el id indicado antes de poder guardar una foto.')                

        try:
            buffer = bytes(dataUser.base64Image, 'utf-8')
            imageBase64 = buffer[buffer.find(b'/9'):]
            imageBytes = io.BytesIO(base64.b64decode(imageBase64))
            faceImage = FaceRec.find_face_image(imageBytes)
            image = Image.open(imageBytes)

            if faceImage is None:
                raise RuntimeError('No se ha detectado ningún rostro en la foto. Favor de volver a intentarlo.')

            absPath = os.path.abspath("app/faces/reconocimiento")
            directoryImages = ""

            if user['snVisitante']:
                directoryImages = f'app/faces/visitantes/{dataUser.id}'
            else:
                directoryImages = f'app/faces/usuarios/{dataUser.numControl}'


            if not os.path.exists(directoryImages):
                raise RuntimeError('No se han encontrado imagenes del rostro del usuario. Favor de validar.')

            # Reconocimiento
            now = datetime.now()
            current_time = now.strftime("%Y%m%d%H%M%S")
            temp_name = next(tempfile._get_candidate_names())

            imageName = f'rec_{current_time}_{temp_name}.jpg';
            pathImage = f'{absPath}/{imageName}'
            image.save(pathImage)

            print(pathImage)            

            isFaceRecognized = FaceRec.recognize_face(pathImage, directoryImages)

            if not isFaceRecognized:
                raise RuntimeError('El rostro indentificado no coincide con los registros del usuario TECN indicado.')

            connection = self.dbManager.obtener_conexion()
            fechaAcceso = now.strftime("%Y-%m-%d %H:%M:%S")
            acceso = self.dbManager.registrar_acceso_usuario(connection, dataUser, fechaAcceso)

            return acceso
        except Exception as ex:
            raise ex;
        finally:
            if(image is not None):
                image.close()

    def inicializar_directorios_registro(self):
        directoriosRegistro = [
            'app/faces/visitantes',
            'app/faces/usuarios',
            'app/faces/reconocimiento'
        ]

        for directory in directoriosRegistro:
            Utils.create_directory(directory)